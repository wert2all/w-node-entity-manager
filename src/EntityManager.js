'use strict';
const EMEntity = require('w-node-db-entity');

/**
 * @class EMEntityManager
 * @type EMEntityManager
 */
class EMEntityManager {

    /**
     *
     * @param {DBConnectionInterface} connection
     */
    constructor(connection) {
        /**
         *
         * @type {DBConnectionInterface}
         * @private
         */
        this._connection = connection;
    }

    /**
     *
     * @param {DBModelInterface} model
     * @param {{}} data
     * @return {EMEntity}
     */
    create(model, data,) {
        return new EMEntity(
            model.create(data),
            data
        );
    }

    /**
     *
     * @param {EMEntity} entity
     * @return {Promise<EMEntity>}
     */
    save(entity) {
        return this._saveChildren(entity)
            .then(entity => this._addChildren(entity))
            .then(entity => this._saveEntity(entity));
    }

    /**
     *
     * @param {EMEntity} entity
     * @returns {Promise<EMEntity>}
     * @private
     */
    _saveEntity(entity) {
        const data = {};
        const entityData = entity.getData();

        entityData
            .getKeys()
            .forEach(key =>
                data[key] = entityData.getData(key)
            );

        return entity
            .getModel()
            .save(data)
            .then((model) => {
                data[model.getModelDefinition().getPrimaryColumnName()] =
                    model.getPrimaryKeyValue();
                return new EMEntity(model, data);
            });
    }

    /**
     *
     * @param {EMEntity} entity
     * @return {Promise<EMEntity>}
     * @private
     */
    _saveChildren(entity) {
        return Promise
            .all(
                this._createPromisesForChildren(entity.getChildren())
            )
            .then(() => entity);
    }

    /**
     *
     * @param {EMEntity} entity
     * @return {Promise<EMEntity>}
     * @private
     */
    _addChildren(entity) {
        return new Promise(resolve => {
            entity
                .getChildren()
                .map(childEntity =>
                    entity.getModel()
                        .addChild(childEntity.getModel()));
            resolve(entity);
        });
    }

    /**
     *
     * @param {[EMEntity]} children
     * @return {[Promise<EMEntity>]}
     * @private
     */
    _createPromisesForChildren(children) {
        return children
            .map(child => this.save(child));
    }
}

module.exports = EMEntityManager;
