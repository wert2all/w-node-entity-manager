'use strict';

/**
 * The entry point.
 *
 * @module EMEntity-Manager
 */
module.exports = require('./src/EntityManager');
